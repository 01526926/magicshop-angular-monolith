import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Article} from '../model/article';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private static readonly ARTICLE_ENDPOINT = environment.apiUrl + '/article';

  constructor(private http: HttpClient) {
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(ArticleService.ARTICLE_ENDPOINT);
  }
}
