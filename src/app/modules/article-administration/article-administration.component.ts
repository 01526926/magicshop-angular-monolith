import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Article} from './model/article';
import {ArticleService} from './service/article.service';

@Component({
  templateUrl: './article-administration.component.html',
  styleUrls: ['./article-administration.component.scss']
})
export class ArticleAdministrationComponent implements OnInit {
  articles: Article[];
  activeArticle: Article;
  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private articleService: ArticleService) {
  }

  ngOnInit(): void {
    this.articleService.getArticles().subscribe(a => {
      this.articles = a;
      this.activeArticle = this.articles[0];
    });
    this.form = this.formBuilder.group({
      id: [0],
      name: [''],
      price: [''],
      active: [false]
    });
  }

  onRowClick(rowIndex: number) {
    this.setActive(rowIndex);
  }

  onSave() {
    alert('Save data was clicked.');
  }

  private setActive(rowIndex: number) {
    this.activeArticle = this.articles[rowIndex];
  }
}
