import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleAdministrationComponent} from './article-administration.component';

describe('DashboardComponent', () => {
  let component: ArticleAdministrationComponent;
  let fixture: ComponentFixture<ArticleAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArticleAdministrationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
