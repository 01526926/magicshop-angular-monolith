import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ArticleAdministrationComponent} from './article-administration.component';

const routes: Routes = [
  {
    path: '',
    component: ArticleAdministrationComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ArticleAdministrationRoutingModule {
}
