import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private authService: AuthenticationService) {
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onLogin() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    setTimeout(() => {
      this.authService.login({username: this.f.username.value, password: this.f.password.value});
      this.loading = false;
      this.router.navigate(['/dashboard']).then();
    }, 1000);
  }
}
